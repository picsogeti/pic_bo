package com.sogeti.controller;

import java.io.IOException;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sogeti.dto.ProjetDTO;
import com.sogeti.jenkins.ArtifactJenkins;
/**
 * 
 * @author syahiaou
 *
 */
@Controller
@CrossOrigin
@RequestMapping("PIC_BO/jenkins")
@Getter
@Setter
public class ArtifactJenkinsController {
	
	Logger LOGGER = Logger.getLogger(ConsoleJenkinsController.class);
	
	@Autowired
	private ArtifactJenkins artifactJenkins;
	
	//Constructeur par defaut
	public ArtifactJenkinsController() {
		LOGGER.info("ArtifactJenkinsController");
	}
	@ResponseStatus(HttpStatus.OK)
	@CrossOrigin(origins="*",methods = RequestMethod.POST)
	@RequestMapping(value="/artifact",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String>  createJob(@RequestBody ProjetDTO projetDTO) throws IOException {
		
			//On recupere le nom du projet 
	    	String nomProjet = projetDTO.getNomProjet();
	    	//On verifie si le nomProjet n'est pas null et vide 
			if (StringUtils.isNotBlank(nomProjet)) {
				try {
					//On fait appel au service create de la methode ConsoleJenkins 
					String location = getArtifactJenkins().RecupererArtifactJenkins(nomProjet);
					return new ResponseEntity<String>(location, HttpStatus.CREATED);
				} catch (IOException ex) {
					LOGGER.warn(ex.getMessage());
					return new ResponseEntity<String>(ex.getMessage(), HttpStatus.FORBIDDEN);
				}	
			}else {
				return new ResponseEntity<String>("Impossible de recuperer l'artifact de Jenkins", HttpStatus.FORBIDDEN);
			}

	}
}