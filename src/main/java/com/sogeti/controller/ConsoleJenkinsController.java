package com.sogeti.controller;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sogeti.dto.ProjetDTO;
import com.sogeti.jenkins.ConsoleJenkins;
/**
 * 
 * @author syahiaou
 *
 */
@Controller
@CrossOrigin
@RequestMapping("PIC_BO/jenkins")
public class ConsoleJenkinsController {
	
	Logger LOGGER = Logger.getLogger(ConsoleJenkinsController.class);
	
	@Autowired
	private ConsoleJenkins consoleJenkins;
	
	
	/**
	 * @return the consoleJenkins
	 */
	public ConsoleJenkins getConsoleJenkins() {
		return consoleJenkins;
	}

	/**
	 * @param consoleJenkins the consoleJenkins to set
	 */
	public void setConsoleJenkins(ConsoleJenkins consoleJenkins) {
		this.consoleJenkins = consoleJenkins;
	}

	public ConsoleJenkinsController() {
		LOGGER.info("ConsoleJenkinsController");
	}
	@ResponseStatus(HttpStatus.OK)
	@CrossOrigin(origins="*",methods = RequestMethod.POST)
	@RequestMapping(value="/console",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String>  createJob(@RequestBody ProjetDTO projetDTO) throws IOException {
		
			//On recupere le nom du projet 
	    	String nomProjet = projetDTO.getNomProjet();
	    	//On verifie si le nomProjet n'est pas null et vide 
			if (StringUtils.isNotBlank(nomProjet)) {
				try {
					//On fait appel au service create de la methode ConsoleJenkins 
					String location = getConsoleJenkins().RecupererConsoleJenkins(nomProjet);
					return new ResponseEntity<String>(location, HttpStatus.CREATED);
				} catch (IOException ex) {
					LOGGER.warn(ex.getMessage());
					return new ResponseEntity<String>(ex.getMessage(), HttpStatus.FORBIDDEN);
				}	
			}else {
				return new ResponseEntity<String>("Impossible de recuperer la console de Jenkins", HttpStatus.FORBIDDEN);
			}

	}
}