package com.sogeti.controller;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sogeti.dto.ProjetDTO;
import com.sogeti.jenkins.CreateItem;
/**
 * 
 * @author syahiaou
 *
 */
@Controller
@CrossOrigin
@RequestMapping("PIC_BO/jenkins")
public class CreateItemController {

	Logger LOGGER = Logger.getLogger(CreateItemController.class);
	
	@Autowired
	private CreateItem createItem;
	/**
	 * @return the createItem
	 */
	public CreateItem getCreateItem() {
		return createItem;
	}
	/**
	 * @param createItem the createItem to set
	 */
	public void setCreateItem(CreateItem createItem) {
		this.createItem = createItem;
	}
	/**
	 * Constructeur par d�faut.
	 */
	public CreateItemController() {
		LOGGER.info("CreateItemController");
	}
	@ResponseStatus(HttpStatus.OK)
	@CrossOrigin(origins="*",methods = RequestMethod.POST)
	@RequestMapping(value="/create",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String>  createJob(@RequestBody ProjetDTO projetDTO) throws IOException {
		try {
			//TODO recupere toutes les informations nec�ssaire pour configurer le fichier configXML
	    	String nomProjet = projetDTO.getNomProjet();
			String location = getCreateItem().CreateJob(nomProjet);
			return new ResponseEntity<String>(location, HttpStatus.CREATED);
			
		} catch (IOException ex) {
			LOGGER.warn(ex.getMessage());
			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.FORBIDDEN);
		}
	}
}
