/**
 * 
 */
package com.sogeti.controller;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.sogeti.dto.ProjetDTO;
import com.sogeti.jenkins.BuildItem;

/**
 * @author syahiaou
 *
 */
@Controller
@CrossOrigin
@RequestMapping("PIC_BO/jenkins")
public class BuildItemController {
	
	Logger LOGGER = Logger.getLogger(BuildItemController.class);
	
	@Autowired
	private BuildItem buidlItem;
	/**
	 * @return the buidlItem
	 */
	public BuildItem getBuidlItem() {
		return buidlItem;
	}
	/**
	 * @param buidlItem the buidlItem to set
	 */
	public void setBuidlItem(BuildItem buidlItem) {
		this.buidlItem = buidlItem;
	}
	/**
	 * Constructeur par d�faut.
	 */
	public BuildItemController() {
		LOGGER.info("BuildItemController");
	}

	@ResponseStatus(HttpStatus.OK)
	@CrossOrigin(origins="*",methods = RequestMethod.POST)
	@RequestMapping(value="/build",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String>  launchBuild(@RequestBody ProjetDTO projetDTO) throws ClientProtocolException, IOException {
		
		//On recupere le nom du projet 
    	String nomProjet = projetDTO.getNomProjet();
    	//On verifie si le nomProjet n'est pas null et vide 
		if (StringUtils.isNotBlank(nomProjet)) {
			try {
				//On fait appel au service lauchBuild 
				String location = getBuidlItem().launchBuild(nomProjet);
				return new ResponseEntity<String>(location, HttpStatus.CREATED);
			} catch (IOException ex) {
				LOGGER.warn(ex.getMessage());
				return new ResponseEntity<String>(ex.getMessage(), HttpStatus.FORBIDDEN);
			}	
		}else {
			return new ResponseEntity<String>("Impossible de lancer le Build Jenkins", HttpStatus.FORBIDDEN);
		}

	}
}	
