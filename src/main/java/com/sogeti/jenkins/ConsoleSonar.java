package com.sogeti.jenkins;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
/**
 * 
 * @author syahiaou
 *
 */
@Service
public class ConsoleSonar {
	
	// Initialisation du LOGGER
	private static final Logger LOGGER = Logger.getLogger(ConsoleSonar.class);

	//Adresse de jenkins
	private static final String ADDRESS_SONAR = "http://localhost:9000";

	public String RecupererConsoleSonar (String nomProjet) throws IOException {
		LOGGER.info("D�but m�thode : RecupererConsoleSonar");
		//Url de Sonar avec le chemin pour recuperer la console des erreurs 
		String strURL = ADDRESS_SONAR + "/api/issues/search?componentRoots=" + nomProjet;
		GetMethod get = new GetMethod(strURL);
		get.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
		HttpClient httpclient = new HttpClient();

		try {
			//On recupere le code status de la reponse 200 si c'est bon, 400 sinon
            int result = httpclient.executeMethod(get);
            LOGGER.info("Response status code: " + result);
                    
		} catch (Exception e) {
            // Release current connection to the connection pool 
            // once you are done
            get.releaseConnection();
        }
		return get.getResponseBodyAsString();

	}
	
}
