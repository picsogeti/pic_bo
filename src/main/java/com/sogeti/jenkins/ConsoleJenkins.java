package com.sogeti.jenkins;

import java.io.IOException;

import javax.transaction.Transactional;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
/**
 * 
 * @author syahiaou
 *
 */
@Service
public class ConsoleJenkins {
	
	// Initialisation du LOGGER
	private static final Logger LOGGER = Logger.getLogger(ConsoleJenkins.class);
	//Adresse de jenkins
	private static final String ADDRESS_JENKINS = "http://localhost:8083";
	@Transactional
	public String RecupererConsoleJenkins (String nomProjet) throws IOException {
		//Logger
		LOGGER.info("D�but m�thode : RecupererConsoleJenkins");
		//Url de jenkins va nous permettre de recuperer la console de Jenkins
		String strURL = ADDRESS_JENKINS + "/job/" + nomProjet + "/lastBuild/logText/progressiveText?start=0";
		GetMethod post = new GetMethod(strURL);
		post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
		HttpClient httpclient = new HttpClient();
			
		try {
			//On recupere le code status de la reponse 200 si c'est bien pass�, 400 sinon	
            int result = httpclient.executeMethod(post);
            LOGGER.info("Response status code: " + result);
		} catch (Exception e) {
            // Release current connection to the connection pool 
            // once you are done
            post.releaseConnection();
        }
		return post.getResponseBodyAsString();
	}
}
