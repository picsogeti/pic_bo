package com.sogeti.jenkins;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.transaction.Transactional;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
/**
 * 
 * @author syahiaou
 *
 */
@Service
public class CreateItem {
	
	// Initialisation du LOGGER
	private static final Logger LOGGER = Logger.getLogger(ConsoleJenkins.class);
	
	//Adresse de jenkins
	private static final String ADDRESS_JENKINS = "http://localhost:8083";
	@Transactional
	public String CreateJob (String nomProjet) throws FileNotFoundException {
		//Logger
		LOGGER.info("D�but m�thode : CreateJob");
		
		String strURL = ADDRESS_JENKINS + "/createItem?name=" + nomProjet;
		File input = new File("D:/config.xml");
		PostMethod post = new PostMethod(strURL);
		post.setRequestEntity(new InputStreamRequestEntity(new FileInputStream(input), input.length()));
		post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
		HttpClient httpclient = new HttpClient();

		try {

            int result = httpclient.executeMethod(post);
            LOGGER.info("Response status code: " + result);
		} catch (Exception e) {
            // Release current connection to the connection pool 
            // once you are done
            post.releaseConnection();
        }
		return strURL;
		
	}
}